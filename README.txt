
Cacti Integration for Drupal
============================

Requires Drupal 6.x.


WHAT THIS DOES:
This module is designed to enable a Drupal site to be queried by Cacti for
statistics. The data goes one way: from Drupal to Cacti. You look at the
details within the Cacti interface.

WHAT THIS DOES NOT DO:
It does not allow you to embed or otherwise 'bring back' information from
Cacti to Drupal. It's a one way trip.



ARCHITECTURE:
There are multiple pieces to making this all work. Here they are.

1) This module. This module is installed on your Drupal site. It acts as
a 'server' to reply to queries from Cacti. In Cacti-speak, this is the
server side of the Data Input Method. Its only job is to collect information
and provide it to a calling Cacti host/script. It must be enabled on the
Drupal site and then configured in the admin -> reports -> cacti menu.

It will not work if you do not enable it AND configure it!

2) The drupal_stats.php file. This is the 'client' side of the system. This 
file is placed on the Cacti server inside of the scripts directory. Generally
this is a manual step to upload it appropriately to your Cacti instance. The
script is then used to query the Drupal module as described in #1.

3) Cacti templates. The templates are what gets imported into the Cacti
software and defines what the information does, the layout, and the graphs. 
The templates are stored in this module's 'templates' directory and must be
downloaded to your system and then uploaded into Cacti. Generally this would
be a manual step to log in to Cacti, click 'Import Templates', and then
browse to them one at a time and click Import.



ACCESS CONTROL:
Access is based upon IP address. This works under the assumption that the
Cacti instance has one or more static IPs. It does not accept a range or
CIDR masks; only individual IPs listed one at a time. Any request coming
from those IPs will succeed and the module will provide the reply to the
query.

The module also supports SecurePages. This is especially useful since the
access control is based on IP address. If a reverse proxy is in use, Drupal
may see all IPs as 127.0.0.1 or the IP of the proxy. Since HTTPS/SSL is
generally not proxied, it allows you to require SecurePages so the IP access
check will see the actual originating IP and not the proxy. If you are not
using a reverse proxy this probably isn't a big deal for you.

IMPORTANT: If you are using SecurePages, make sure to visit the SecurePages
admin form and enter the path stats/cacti/* as a page that requires SSL.
Otherwise it won't work properly.



CURRENTLY SUPPORTED STATISTICS:
* Guest Users and Authenticated Users



DIRECTORY LAYOUT:
methods/		Server-side 'plugins' to query Drupal for statistics
client/			The client to be placed in the Cacti scripts directory.
templates/	Graph templates to be imported into Cacti.
