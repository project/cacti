<?php

/**
 * @file
 * Cacti Admin Interface
 *
 */

/**
* Implementation of hook_help()
*  Add help text to the admin page
*/
function cacti_help($path, $arg) {
  switch ($path) {
    case 'admin/reports/cacti':
      $message = '<p>' . t('This page allows the Cacti service to be configured. This is the Drupal-side of the module that provides the returned statistics to your Cacti server. There is no standard Drupal user authentication used because your Cacti poller is not going to log in as a user. The access control is only based on source IP address of your Cacti server.');
      $message .= '<p>' . t('This assumes that you pass clean source IP addresses to your Drupal module (i.e. no reverse proxy.) If you use a reverse proxy you may want to configure it to pass the client IP or use SecurePages/HTTPS direct to the server.');
      $message .= '<p>' . t('As a reminder, this module provides a one-way trip: statistics from Drupal to Cacti. No graphs or other information are made available to Drupal - the output graphs are all in Cacti.');
      return $message;
  }
}

/**
* Form function for settings
*
* @return
*   The newly built form
*/
function cacti_settings() {
  $form = array();

  $form['cacti_authentication'] = array(
    '#type'             => 'fieldset',
    '#title'            => t('Access Control'),
  );
  $form['cacti_authentication']['cacti_allowedips'] = array(
    '#type'             => 'textarea',
    '#title'            => t('Source IP addresses of the Cacti server(s), one per line'),
    '#default_value'    => variable_get('cacti_allowedips', NULL),
    '#required'         => TRUE,
    '#description'      => t("Enter one IP per line. If your Cacti server is on the same machine with your Drupal site, you may want to use 127.0.0.1"),
  );

  if (module_exists('securepages')) {
    $form['cacti_authentication']['cacti_requirehttps'] = array(
      '#type'             => 'checkbox',
      '#title'            => t('Require Cacti polling to use SecurePages with HTTPS/TLS'),
      '#default_value'    => variable_get('cacti_requirehttps', NULL),
      '#required'         => TRUE,
      '#description'      => t('This is recommended if reverse proxies are used or source IP is otherwise masked. Also look at <a href="http://stderr.net/apache/rpaf/">Apache mod_rpaf</a> for passing the correct source IP.'),
    ); 
  }
  // If SecurePages is not enabled get rid of the variable requiring it.
  elseif (variable_get('cacti_requirehttps', TRUE)) {
    variable_del('cacti_requirehttps');
  }

  return system_settings_form($form);
}

