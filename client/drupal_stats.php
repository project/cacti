<?php

/**
 * @file
 * Cacti client for Drupal Cacti Integration module
 *   This acts as a client to query the running Cacti Integration module on
 *   a Drupal site. The syntax and usage is as follows:
 *
 *   <path_php_binary> -q <path_cacti>/scripts/drupal_stats.php <hostname> 80 <query>
 *
 *   For me, it looks like this since I use SSL/TLS. The port is manually entered.
 *   <path_php_binary> -q <path_cacti>/scripts/drupal_stats.php <hostname> 443 <query>
 *
 *   Note: The php curl extension is used to obtain the results. This will not work
 *   if curl is not enabled for php on the Cacti server itself (not the Drupal site.)
 */


if ($argc != 4) {
  print "Not enough arguments. Exiting.\n";
  print "Usage: php -q drupal_stats.php <hostname> <drupal port> <query>\n";
  exit (1);
}

$host   = $argv[1];
$port   = $argv[2];
$query  = $argv[3];

if ($port == '443') {
  $proto = "https://";
} 
else {
  $proto = "http://";
}

$drupal_cactiurl = $proto . $host . "/stats/cacti/" . $query;
$fp = fopen('/tmp/' . $host . '_drupal-' . $query . '.txt', 'w');

$drupal_statistics = get_drupal_statistics($drupal_cactiurl);

print $drupal_statistics;

fwrite($fp, $drupal_statistics);
fclose($fp);


/**
 * @input $url
 *   URL to query for statistics
 * @return
 *   Statistical output
 */
function get_drupal_statistics ($url) {
  $curl_handler = curl_init ();
  // CURL options
  curl_setopt($curl_handler, CURLOPT_URL, $url);
  curl_setopt($curl_handler, CURLOPT_HEADER, FALSE);
  curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl_handler, CURLOPT_SSL_VERIFYPEER, FALSE);
  
  // Ensure there are no trailing spaces in the output.
  $curl_output = trim(curl_exec($curl_handler));
  return $curl_output;
}

?>
